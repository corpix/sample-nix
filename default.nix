{ pkgs ? (import <nixpkgs> {}) }:
with pkgs; stdenv.mkDerivation rec {
  name = "sample-app-${version}";
  version = "0";
  src = lib.sourceFilesBySuffices ./. [".c" "Makefile"];

  configurePhase = "";
  buildPhase = ''
    make -C $src build
  '';
  installPhase = ''
    mkdir -p $out/bin
    find . -executable -type f -exec mv {} $out/bin \;
  '';
}
