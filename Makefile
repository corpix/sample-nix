.DEFAULT_GOAL := all

name              := sample-app
container_archive := container.tar.gz

shell = $(shell if which nix-build > /dev/null 2>&1; then true; else echo ./shell; fi)

IMAGE_NAME ?= corpix/$(name)
IMAGE_TAG  ?= latest

all: build

.PHONY: build
build: $(name)

$(name): main.c
	$(CC) -o $(PWD)/$@ $<

.PHONY: run
run: $(name)
	$<

##

.PHONY: shell
shell:
	./shell

##

.PHONY: $(container_archive)
$(container_archive): container.nix
	$(shell) nix-build --expr 'import ./$< { name = "$(IMAGE_NAME)"; tag = "$(IMAGE_TAG)"; }'
	$(shell) cp -Lf --preserve=timestamps result ./$@
	$(shell) nix-store --delete --ignore-liveness result
	rm -f result

container: $(container_archive)
	docker load < $<
