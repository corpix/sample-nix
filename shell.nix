let
  nixpkgs = builtins.fetchTarball {
    url    = "https://github.com/nixos/nixpkgs/archive/03050e9749e1548f1648aae5c062c954eaad546e.tar.gz";
    sha256 = "00hfkldmf853ynnd8a9d7a778ifcrdjxdndxyykzbpxfki5s5qsb";
  };
in with import nixpkgs {};
let
  # tmux starts as a login shell
  # bash reads /etc/profile in this mode
  # /etc/profile resets PATH which is a big nono
  # in a nix-shell.
  # this script wraps bash to prevent such behaviour
  bash-wrapper = writeScript "shell-wrapper" ''
    #! ${stdenv.shell}
    export PS1="\e[0;34m\u@\h \w $ \e[m"
    exec -a bash ${bashInteractive}/bin/bash --noprofile "$@"
  '';
  overlay = import ./overlay.nix { inherit pkgs; };
in stdenv.mkDerivation {
  name = "nix-shell";
  buildInputs = with pkgs; with overlay; [
    git
    gnumake
    bashInteractive
    jq
    docker

    sample-app
  ];
  shellHook = ''
    root=$(git rev-parse --show-toplevel)

    export SHELL="${bash-wrapper}"
    export NIX_PATH=nixpkgs=${nixpkgs}
  '';
}
