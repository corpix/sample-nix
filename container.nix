{ pkgs ? import <nixpkgs> {}, name, tag }:
let
  overlay = import ./overlay.nix { inherit pkgs; }; 
in pkgs.dockerTools.buildImage {
  inherit name tag;

  contents = with pkgs; with overlay; [sample-app busybox];

  config = with overlay; {
    Volumes = { "/data" = {}; };
    Env = [
      "HELLO=1"
    ];
    Cmd = [
      "/bin/sample-app"
    ];
    WorkDir = "/data";
  };
}
