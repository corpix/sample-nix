sample-app
----

## development

From the root of this repo run:

> It will require a `docker` and could require `sudo` in some cases.

```console
$ make shell
...
bash-4.4# make run
```

## container

You could build a container with this command:

```console
$ make container
```

## build support

- `default.nix` declares how to build a single binary from the sources in this repository
- `container.nix` declares how to build a container from `default.nix`
- `shell.nix` declarres an environment for development
- `shell` is a script which starts nix-shell encvironment inside docker container
- `Makefile` declares targets to build application, build container and run application

### update dependencies versions

Any system packages(like `make` or `docker` client) is installed from [nixpkgs](https://github.com/nixos/nixpkgs) which is a repository with software "sets". You could find a specific `nixpkgs` revision
on the top of `shell.nix`. Nixpkgs has rolling releases, to update:

- go to the [hydra](https://hydra.nixos.org/jobset/nixpkgs/trunk)(nix buildfarm)
- locate a fresh revision in the «input changes» column with lowest build fails(it is ok to have 2-3k fails)
- update revision in the `shell.nix` and run `shell`, it will fail because of hash mistmatch
- new hash will be in the error message
- update the sha256 in `shell.nix`
- re-run `shell` and it will download new versions of software
